#!/usr/bin/env bash

set -e

# Install ADB and Fastboot drivers
sudo apt install -y android-tools-adb android-tools-fastboot
