#!/usr/bin/env bash

set -e

# Add Atom repository
curl -fsSL "https://packagecloud.io/AtomEditor/atom/gpgkey" | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any any main"
sudo apt update

# Install Atom
sudo apt install -y atom
