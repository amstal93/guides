#!/usr/bin/env bash

set -e

# Add Docker repository
curl -fsSL "https://download.docker.com/linux/ubuntu/gpg" | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update

# Install Docker
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose

# Add the user to the 'docker' group
sudo usermod -aG docker $USER

echo -e "\nYou have to reboot to apply the changes"
