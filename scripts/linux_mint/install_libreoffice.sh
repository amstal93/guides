#!/usr/bin/env bash

set -e

# Add LibreOffice repository
sudo add-apt-repository -y ppa:libreoffice/ppa
sudo apt update

# Update the system
echo -ne "\nDo you want to update the system? [y/N] "
read foo
if [[ $foo =~ ^[yY]$ ]]; then
	sudo apt upgrade -y
fi

# Install Microsoft's fonts
echo -ne "\nDo you want to install Microsoft fonts? [y/N] "
read foo
if [[ $foo =~ ^[yY]$ ]]; then
	sudo apt install -y ttf-mscorefonts-installer
fi
