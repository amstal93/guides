#!/usr/bin/env bash

set -e

# Install 'xclip'
sudo apt install -y xclip

# Downlod the '.deb' file from https://github.com/zyedidia/micro/releases/latest
# Do NOT install 'micro' with 'apt install micro', as it's outdated and has a
# known bug where debug mode is enabled
curl -LO $(curl -s "https://api.github.com/repos/zyedidia/micro/releases/latest" | grep "browser_" | grep ".deb" | cut -d \" -f4)
sudo apt install ./micro-*.deb
rm micro-*.deb

# Edit the settings
mkdir -p "$HOME/.config/micro/"
cat > "$HOME/.config/micro/settings.json" << EOL
{
    "colorcolumn": 80,
    "diffgutter": true,
    "rmtrailingws": true,
    "savecursor": true,
    "scrollbar": true,
    "tabstospaces": false
}
EOL

# Install some plugins
micro -plugin install snippets
micro -plugin install filemanager
micro -plugin install manipulator
micro -plugin install quoter
