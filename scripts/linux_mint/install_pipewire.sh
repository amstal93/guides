#!/usr/bin/env bash

set -e

# Add PipeWire repository
sudo add-apt-repository -y ppa:pipewire-debian/pipewire-upstream
sudo apt update

# Install PipeWire
sudo apt install -y pipewire pipewire-doc gstreamer1.0-pipewire libspa-0.2-bluetooth libspa-0.2-jack pipewire-audio-client-libraries

# Populate the 'media-session.d' folder
sudo mkdir -p /etc/pipewire/media-session.d/
sudo touch /etc/pipewire/media-session.d/with-alsa
sudo touch /etc/pipewire/media-session.d/with-jack
sudo touch /etc/pipewire/media-session.d/with-pulseaudio

# Copy the configuration for ALSA and JACK
sudo cp /usr/share/doc/pipewire/examples/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d/
sudo cp /usr/share/doc/pipewire/examples/ld.so.conf.d/pipewire-jack-*.conf /etc/ld.so.conf.d/
sudo ldconfig

echo -e "\nYou have to reboot to apply the changes!"
