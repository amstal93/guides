#!/usr/bin/env bash

set -e

# Remove 'onboard', 'mintwelcome', 'redshift' and 'hexchat'
sudo apt purge -y onboard mintwelcome redshift hexchat

# Remove the Gnome Software Manager
echo -ne "\nDo you want to remove the Gnome Software Manager? [y/N] "
read foo
if [[ $foo =~ ^[yY]$ ]]; then
	sudo apt purge -y mintinstall
fi

# Install 'samba' 'tlp' 'sane-airscan' 'python3-pip'
sudo apt install -y samba tlp sane-airscan python3-pip

echo -e "\nYou have to reboot to apply the changes!"
