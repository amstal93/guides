# Install Custom ROMs on the Xiaomi Mi A2 Lite

A guide on how to install a Custom ROM on the Xiaomi Mi A2 Lite (codename: Daisy)

### Prerequisites:

* ADB and Fastboot drivers

* [Official 10.0.3.0 firmware](https://bigota.d.miui.com/V10.0.3.0.PDLMIXM/daisy_global_images_V10.0.3.0.PDLMIXM_20190114.0000.00_9.0_e8d8d4a6d0.tgz/)

* Offain's ([@33bca](https://github.com/33bca/)) TWRP [.img](https://androidfilehost.com/?fid=6006931924117887098/) and [.zip](https://androidfilehost.com/?fid=6006931924117887097/)

* [ForceEncryption Disabler](https://androidfilehost.com/?fid=1395089523397907941/)

* A Custom ROM .zip (for example, [LineageOS 18.1](https://github.com/a-huk/otaserver/releases/latest/))

* [Magisk](https://github.com/topjohnwu/Magisk/releases/latest/), [GApps](https://opengapps.org/) or everything you want to install on your phone

### Index:

1. [Unlock the Bootloader](#1-unlock-the-bootloader)

2. [Flash the Stock Firmware](#2-flash-the-stock-firmware)

3. [Flash the Custom ROM](#3-flash-the-custom-rom)

## 1. Unlock the Bootloader

First of all, we need to unlock the bootloader

**This procedure will delete all your data, so make a backup before you begin**

* Go to `Settings > About Phone` and tap `Build Number` seven times to become a developer

* Then, go to `Settings > System > Developer Option` and enable `OEM Unlock` and `USB Debugging`

* Turn off the phone, wait 3 seconds and then press `Volume Down` and `Power` at the same time, until you see the fastboot logo

* Immediately connect the phone to the computer

* Open a terminal and type

  ```
  sudo fastboot oem unlock
  ```

* Wait untill the phone reboots

## 2. Flash the Stock Firmware

* Re-enter in the fastboot mode, by pressing `Volume Down` and `Power` at the same time while the phone is off

* Connect the phone to the computer

* Open a terminal in the folder with the stock firmware and type

  ```
  sudo ./flash_all.sh
  ```

* Wait untill the phone reboots

## 3. Flash the Custom ROM

* After the phone has rebooted successfully, turn it off and go into the fastboot mode again

* Connect the phone to the computer

* Open a terminal in the folder where you put all the prerequisites and type

  ```
  sudo fastboot -w
  sudo fastboot boot twrp-daisy-3.3.1-0-offain.img
  ```

* Once you're into the TWRP, type on your computer

  ```
  adb push *.zip /sdcard
  ```

* On the phone, tap `Install` and flash the .zip in this order

  * Custom ROM

  * TWRP (twrp-installer-daisy-3.3.1-0-offain.zip)

  * ForceEncryption Disabler (ForcedEncryptionDisablerDaisy.zip)

* **Don't install Magisk, GApps or anything else for now**

* After everything is done, go back to the main menu and tap on `Reboot`

* **Don't reboot yet**, instead change the slot (for example, if it says `Current Slot: A` tap on `Slot B` and vice versa)

* Go back to the main menu and tap on `Reboot`, then click `Recovery`

* Now you can flash everything you like (Magisk, GApps, etc...)

* Reboot your phone
